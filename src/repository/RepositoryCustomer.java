/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import domain.Customer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author saska
 */
public class RepositoryCustomer {
   private final List<Customer> customers;

    public RepositoryCustomer() {
    
        customers= new ArrayList<>();
    }
    
    public void add(Customer customer){
        customers.add(customer);
    }

    public List<Customer> getCustomers() {
        return customers;
    }
    
    public void delete(Customer customer) throws Exception{
        int index= customers.indexOf(customer);
            if(index>=0){
                customers.remove(index);
            }else{
                throw new Exception("Greška! Kupac ne postoji!");
            }
    }

    public Customer getCustomerById(int customerId) throws Exception {
        for (Customer customer : customers) {
            if(customer.getCustomerId()==customerId){
             return customer;
            }
        }
        throw new Exception("Kupac ne postoji u listi!");
    }

  
    
    
}
