/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import domain.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author saska
 */
public class RepositoryUser {
    List<User> users;

    public RepositoryUser() {
        this.users= new ArrayList<User>();
        users.add(new User(1l, "Admin", "Admin", "admin", "admin", "admin@"));
        users.add(new User(2l, "Admin1", "Admin1", "admin1", "admin1", "admin1@"));
        users.add(new User(3l, "Admin2", "Admin2", "admin2", "admin2", "admin2@"));
    }

    public List<User> getUsers() {
        return users;
    }
    
    
}
