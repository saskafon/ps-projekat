/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import domain.Model;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author saska
 */
public class RepositoryModel {
    private final List<Model> models;

    public RepositoryModel() {
        models=new ArrayList<Model>(){
            {
            add(new Model(1, "Fabia"));
            add(new Model(1, "Scala"));
            add(new Model(1, "Superb"));
            add(new Model(1, "Kamiq"));
            add(new Model(1, "Karoq"));
            add(new Model(1, "Kodiaq"));
            add(new Model(1, "Octavia"));
            }
        };
    }

    public List<Model> getModels() {
       return models;
    }
}
