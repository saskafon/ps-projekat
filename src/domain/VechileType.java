/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author saska
 */
public enum VechileType {
    LIMOUSINE{
        public String toString() {
            return "Limuzina";
        }
    },
    HATCHBACK{
    public String toString() {
            return "Hečbek";
        }
    }
    ,
    ESTATE{
    public String toString() {
            return "Karavan";
        }
    },
    COUPE{
    public String toString() {
            return "Kupe";
        }
    },
    CABRIOLET{
    public String toString() {
            return "Kabriolet";
        }
    },
    MPV{
    public String toString() {
            return "Monovolumen";
        }
    },
    SUV{
    public String toString() {
            return "Džip";
        }
    },
    PICKUP{
    public String toString() {
            return "Pikap";
        }
    }
}
