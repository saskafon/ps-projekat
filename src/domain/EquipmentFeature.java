/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author saska
 */
public class EquipmentFeature {
    private int autommobileId;
    private int equipmentId;
    private String name;
    private BigDecimal priceWithVat; //cenaSaPDV-om
    private BigDecimal priceWithoutVat; //cenaBezPDV-a
    private int equipmentStatus;

    public EquipmentFeature() {
    }

    public EquipmentFeature(int autommobileId, int equipmentId, String name, BigDecimal priceWithVat, BigDecimal priceWithoutVat, int equipmentStatus) {
        this.autommobileId = autommobileId;
        this.equipmentId = equipmentId;
        this.name = name;
        this.priceWithVat = priceWithVat;
        this.priceWithoutVat = priceWithoutVat;
        this.equipmentStatus = equipmentStatus;
    }

    public int getAutommobileId() {
        return autommobileId;
    }

    public void setAutommobileId(int autommobileId) {
        this.autommobileId = autommobileId;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(int equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPriceWithVat() {
        return priceWithVat;
    }

    public void setPriceWithVat(BigDecimal priceWithVat) {
        this.priceWithVat = priceWithVat;
    }

    public BigDecimal getPriceWithoutVat() {
        return priceWithoutVat;
    }

    public void setPriceWithoutVat(BigDecimal priceWithoutVat) {
        this.priceWithoutVat = priceWithoutVat;
    }

    public int getEquipmentStatus() {
        return equipmentStatus;
    }

    public void setEquipmentStatus(int equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.autommobileId;
        hash = 29 * hash + this.equipmentId;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.priceWithVat);
        hash = 29 * hash + Objects.hashCode(this.priceWithoutVat);
        hash = 29 * hash + this.equipmentStatus;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EquipmentFeature other = (EquipmentFeature) obj;
        if (this.autommobileId != other.autommobileId) {
            return false;
        }
        if (this.equipmentId != other.equipmentId) {
            return false;
        }
        if (this.equipmentStatus != other.equipmentStatus) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.priceWithVat, other.priceWithVat)) {
            return false;
        }
        if (!Objects.equals(this.priceWithoutVat, other.priceWithoutVat)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EquipmentFeature{" + "autommobileId=" + autommobileId + ", equipmentId=" + equipmentId + ", name=" + name + ", priceWithVat=" + priceWithVat + ", priceWithoutVat=" + priceWithoutVat + ", equipmentStatus=" + equipmentStatus + '}';
    }
    
    
}
