/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Objects;

/**
 *
 * @author saska
 */
public class City {
    private int postalCode;
    private String cityName;

    public City() {
    }

    public City(int postalCode, String cityName) {
        this.postalCode = postalCode;
        this.cityName = cityName;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.postalCode;
        hash = 23 * hash + Objects.hashCode(this.cityName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final City other = (City) obj;
        if (this.postalCode != other.postalCode) {
            return false;
        }
        if (!Objects.equals(this.cityName, other.cityName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "City{" + "postalCode=" + postalCode + ", cityName=" + cityName + '}';
    }

    
}
