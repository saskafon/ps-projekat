/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author saska
 */
public enum Fuel {
    DIESEL{
     public String toString() {
            return "Dizel";
        }
    },
    PETROL{
     public String toString() {
            return "Benzin";
        }
    },
    HYBRYD{
     public String toString() {
            return "Hibrid";
        }
    },
    LPG{
     public String toString() {
            return "TNG";
        }
    },
    NG{
     public String toString() {
            return "CNG";
        }
    },
    ELECTRIC{
     public String toString() {
            return "Električni";
        }
    }
}
