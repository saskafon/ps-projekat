/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Objects;

/**
 *
 * @author saska
 */
public class LegalPerson {
    private int customerId;
    private String companyName;
    private String tin; //PIB
    private String companyNumber; //MB-Maticni broj firme

    public LegalPerson() {
    }

    public LegalPerson(int customerId, String companyName, String tin, String companyNumber) {
        this.customerId = customerId;
        this.companyName = companyName;
        this.tin = tin;
        this.companyNumber = companyNumber;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.customerId;
        hash = 97 * hash + Objects.hashCode(this.companyName);
        hash = 97 * hash + Objects.hashCode(this.tin);
        hash = 97 * hash + Objects.hashCode(this.companyNumber);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LegalPerson other = (LegalPerson) obj;
        if (this.customerId != other.customerId) {
            return false;
        }
        if (!Objects.equals(this.companyName, other.companyName)) {
            return false;
        }
        if (!Objects.equals(this.tin, other.tin)) {
            return false;
        }
        if (!Objects.equals(this.companyNumber, other.companyNumber)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LegalEntity{" + "customerId=" + customerId + ", companyName=" + companyName + ", tin=" + tin + ", companyNumber=" + companyNumber + '}';
    }
    
}
