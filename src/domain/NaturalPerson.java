/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Objects;

/**
 *
 * @author saska
 */
public class NaturalPerson {
    private int customerId;
    private String fullName;
    private String personalNumber;

    public NaturalPerson() {
    }

    public NaturalPerson(int customerId, String fullName, String personalNumber) {
        this.customerId = customerId;
        this.fullName = fullName;
        this.personalNumber = personalNumber;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.customerId;
        hash = 59 * hash + Objects.hashCode(this.fullName);
        hash = 59 * hash + Objects.hashCode(this.personalNumber);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NaturalPerson other = (NaturalPerson) obj;
        if (this.customerId != other.customerId) {
            return false;
        }
        if (!Objects.equals(this.fullName, other.fullName)) {
            return false;
        }
        if (!Objects.equals(this.personalNumber, other.personalNumber)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NaturalPerson{" + "customerId=" + customerId + ", fullName=" + fullName + ", personalNumber=" + personalNumber + '}';
    }
    
    
}
