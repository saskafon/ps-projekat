/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author saska
 */
public class Contract {
    private int customerId;
    private int automobileId;
    private Date conclusionDate; //Datum zakljucenja ugovora
    private String note;//napomena

    public Contract() {
    }

    public Contract(int customerId, int automobileId, Date conclusionDate, String note) {
        this.customerId = customerId;
        this.automobileId = automobileId;
        this.conclusionDate = conclusionDate;
        this.note = note;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getAutomobileId() {
        return automobileId;
    }

    public void setAutomobileId(int automobileId) {
        this.automobileId = automobileId;
    }

    public Date getConclusionDate() {
        return conclusionDate;
    }

    public void setConclusionDate(Date conclusionDate) {
        this.conclusionDate = conclusionDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.customerId;
        hash = 79 * hash + this.automobileId;
        hash = 79 * hash + Objects.hashCode(this.conclusionDate);
        hash = 79 * hash + Objects.hashCode(this.note);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contract other = (Contract) obj;
        if (this.customerId != other.customerId) {
            return false;
        }
        if (this.automobileId != other.automobileId) {
            return false;
        }
        if (!Objects.equals(this.note, other.note)) {
            return false;
        }
        if (!Objects.equals(this.conclusionDate, other.conclusionDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Contract{" + "customerId=" + customerId + ", automobileId=" + automobileId + ", conclusionDate=" + conclusionDate + ", note=" + note + '}';
    }
    
    
}
