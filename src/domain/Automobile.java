/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;


import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author saska
 */
public class Automobile {
    private int automobileId;
    private int cubicCapacity; //zapreminaMotora
    private int enginePower; //sangaMotora
    private BigDecimal priceWithVat; //cenaSaPDV-om
    private BigDecimal priceWithoutVat; //cenaBezPDV-a
    private VechileType vechileType; //karoserija
    private Currency currency; 
    private Fuel fuel;
    private int modelId;

    public Automobile() {
    }

    public Automobile(int automobileId, int cubicCapacity, int enginePower, BigDecimal priceWithVat, BigDecimal priceWithoutVat, VechileType vechileType, Currency currency, Fuel fuel, int modelId) {
        this.automobileId = automobileId;
        this.cubicCapacity = cubicCapacity;
        this.enginePower = enginePower;
        this.priceWithVat = priceWithVat;
        this.priceWithoutVat = priceWithoutVat;
        this.vechileType = vechileType;
        this.currency = currency;
        this.fuel = fuel;
        this.modelId = modelId;
    }

    public int getAutomobileId() {
        return automobileId;
    }

    public void setAutomobileId(int automobileId) {
        this.automobileId = automobileId;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public BigDecimal getPriceWithVat() {
        return priceWithVat;
    }

    public void setPriceWithVat(BigDecimal priceWithVat) {
        this.priceWithVat = priceWithVat;
    }

    public BigDecimal getPriceWithoutVat() {
        return priceWithoutVat;
    }

    public void setPriceWithoutVat(BigDecimal priceWithoutVat) {
        this.priceWithoutVat = priceWithoutVat;
    }

    public VechileType getVechileType() {
        return vechileType;
    }

    public void setVechileType(VechileType vechileType) {
        this.vechileType = vechileType;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.automobileId;
        hash = 59 * hash + this.cubicCapacity;
        hash = 59 * hash + this.enginePower;
        hash = 59 * hash + Objects.hashCode(this.priceWithVat);
        hash = 59 * hash + Objects.hashCode(this.priceWithoutVat);
        hash = 59 * hash + Objects.hashCode(this.vechileType);
        hash = 59 * hash + Objects.hashCode(this.currency);
        hash = 59 * hash + Objects.hashCode(this.fuel);
        hash = 59 * hash + this.modelId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Automobile other = (Automobile) obj;
        if (this.automobileId != other.automobileId) {
            return false;
        }
        if (this.cubicCapacity != other.cubicCapacity) {
            return false;
        }
        if (this.enginePower != other.enginePower) {
            return false;
        }
        if (this.modelId != other.modelId) {
            return false;
        }
        if (!Objects.equals(this.priceWithVat, other.priceWithVat)) {
            return false;
        }
        if (!Objects.equals(this.priceWithoutVat, other.priceWithoutVat)) {
            return false;
        }
        if (this.vechileType != other.vechileType) {
            return false;
        }
        if (this.currency != other.currency) {
            return false;
        }
        if (this.fuel != other.fuel) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Automobile{" + "automobileId=" + automobileId + ", cubicCapacity=" + cubicCapacity + ", enginePower=" + enginePower + ", priceWithVat=" + priceWithVat + ", priceWithoutVat=" + priceWithoutVat + ", vechileType=" + vechileType + ", currency=" + currency + ", fuel=" + fuel + ", modelId=" + modelId + '}';
    }
    
}
