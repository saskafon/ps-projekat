/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Automobile;
import domain.Customer;
import domain.Model;
import domain.User;
import java.util.List;
import repository.RepositoryCustomer;
import repository.RepositoryModel;
import repository.RepositoryUser;

/**
 *
 * @author saska
 */
public class Controller {
    private final RepositoryUser repositoryUser;
    private final RepositoryCustomer repositoryCustomer;
    private final RepositoryModel repositoryModel;


    private static Controller controller;
    
    private User loggedUser= new User();
    

    private Controller() {
        this.repositoryUser = new RepositoryUser();
        this.repositoryCustomer= new RepositoryCustomer();
        this.repositoryModel= new RepositoryModel();
       
    }
    
    public static Controller getInstance(){
        if(controller==null){
            controller= new Controller();
        }
        return controller;
    }
    
    public User login(String username, String password) throws Exception{
    List<User> users= repositoryUser.getUsers();
        for (User user : users) {
            if(user.getUserName().equals(username) && user.getPassword().equals(password)){
                loggedUser=user;
                return user;
            }
        }
        throw new Exception("Sistem ne može da nađe korisnika po zadatoj vrednosti!");
     }
    
    public Boolean isCustomerAdded(int id){
    List<Customer> customers= repositoryCustomer.getCustomers();
        for (Customer customer : customers) {
            if(customer.getCustomerId()== id){
            return true;
            }
        }
       return false;
    }
    
    public List<User> getAllUsers(){
    return repositoryUser.getUsers();
    }
    
    public void addCustomer(Customer customer){
        repositoryCustomer.add(customer);
    }
    
    public List<Customer> getAllCustomers() {
        return repositoryCustomer.getCustomers();
    }

    public User getLoggedUser() {
        return loggedUser;
    }
    public void deleteCustomer(Customer customer) throws Exception{
        repositoryCustomer.delete(customer);
    }

    public Customer getCustomerById(int customerId) throws Exception {
        return repositoryCustomer.getCustomerById(customerId);
     
    }

    public List<Model> getAllModels() {
       return repositoryModel.getModels();
    }


   
    
    
    
    
}
