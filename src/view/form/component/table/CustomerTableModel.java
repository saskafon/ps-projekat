/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.form.component.table;

import domain.Customer;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author saska
 */
public class CustomerTableModel extends AbstractTableModel{
    private String[] columnNames ={"Id", "Ime","Prezime","Telefon","Adresa","Email","Postanski broj"};
    private final List<Customer> customers;

    public CustomerTableModel(List<Customer> customers) {
        this.customers = customers;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex==1);
    }
    
    

    @Override
    public String getColumnName(int column) {
        if(column>columnNames.length){
        return "n/a";  //ako neko na primer unese 10
        }
        return columnNames[column];
    }

    @Override
    public void setValueAt(Object Value, int rowIndex, int columnIndex) {
        Customer customer= customers.get(rowIndex);
        switch(columnIndex){
            case 1: 
                customer.setFirstName(String.valueOf(Value));
                break;
        }
    }
    
    
    
    @Override
    public int getRowCount() {
        if(customers==null){
            return 0;
        }
       return customers.size();
    }

    @Override
    public int getColumnCount() {
        return 7; //broj atributa u Cuistomeru
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Customer customer = customers.get(rowIndex);
        switch(columnIndex){
            case 0: return customer.getCustomerId();
            case 1: return customer.getFirstName();
            case 2: return customer.getLastName();
            case 3: return customer.getPhoneNumber();
            case 4: return customer.getEmail();
            case 5: return customer.getAddress();
            case 6: return customer.getPostalCode();
            default: return "n/a";
        }
    }

    public Customer getCustomerAt(int row) {
            return customers.get(row);
    }
    
}
